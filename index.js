
const express = require('express');
const app = express();
const PORT = 4000;

let users = [
	{
		"username": "johndoe",
		"password": "jd1234"
	},

	{
		"username": "janedoe",
		"password": "jane1234"
	}
];

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.get("/home", (request, response) => response.send("Welcome!"));

app.get("/users", (request, response) => {
	response.send(users);

});

app.delete("/delete-user", (request, response) => {
	for(let i=0; i<users.length; i++){
		if(users[i].username == request.body.username){
			response.send(`${users[i].username} has been deleted`);
			users.splice(i, 1);
		}
	}
	
});


app.listen(PORT, () => console.log(`Server running at port ${PORT}`));

